package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.ArrayList;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener, MouseListener {

    private List<Balloon> balloons;
//    private  JButton moveButton;
    private Timer movementTimer;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        this.balloons = new ArrayList<>();

        balloons.add(new Balloon(30, 60));


//        this.moveButton = new JButton("Move balloon");
//        this.moveButton.addActionListener(this);
//        this.add(moveButton);
        this.addKeyListener(this);

        this.movementTimer = new Timer(200, this);

        this.addMouseListener(this);

    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        for (Balloon b: balloons) {
            b.move(this.getSize());
        }

        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (Balloon b: balloons) {
            b.draw(g);
        }
        
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (!movementTimer.isRunning()) {
            movementTimer.start();
        }

        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                for (Balloon b: balloons) {
                    b.setDirection(Direction.Up);
                }
                break;
            case KeyEvent.VK_DOWN:
                for (Balloon b: balloons) {
                    b.setDirection(Direction.Down);
                }
                break;
            case KeyEvent.VK_LEFT:
                for (Balloon b: balloons) {
                    b.setDirection(Direction.Left);
                }
                break;
            case KeyEvent.VK_RIGHT:
                for (Balloon b: balloons) {
                    b.setDirection(Direction.Right);
                }
                break;
            case KeyEvent.VK_S:
                movementTimer.stop();
                break;
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Point clickLocation = e.getPoint();
        balloons.add(new Balloon(clickLocation.x, clickLocation.y));
        this.repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}