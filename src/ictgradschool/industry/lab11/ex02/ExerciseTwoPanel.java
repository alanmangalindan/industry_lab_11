package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener{
    JTextField num1Text, num2Text, resultText;
    JButton addButton, subtractButton;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        num1Text = new JTextField(10);
        num2Text = new JTextField(10);
        addButton = new JButton("Add");
        subtractButton = new JButton("Subtract");
        resultText = new JTextField(20);

        JLabel resultLabel = new JLabel("Result: ");

        this.add(num1Text);
        this.add(num2Text);
        this.add(addButton);
        this.add(subtractButton);
        this.add(resultLabel);
        this.add(resultText);

        addButton.addActionListener(this);
        subtractButton.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String num1String = num1Text.getText();
        String num2String = num2Text.getText();

        // avoid NullPointerException:
        if (num1String.equals("")) num1String = "0";
        if (num2String.equals("")) num2String = "0";

        double num1, num2;

        try {
            num1 = Double.parseDouble(num1String);
            num2 = Double.parseDouble(num2String);
        } catch (NumberFormatException e) {
            System.out.println("Error: " + e.getMessage());
            return;
        }

        if (event.getSource() == addButton) {
            double result = num1 + num2;
            resultText.setText(roundTo2DecimalPlaces(result) + "");
        }
        else if (event.getSource() == subtractButton) {
            double result = num1 - num2;
            resultText.setText(roundTo2DecimalPlaces(result) + "");
        }
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }


}