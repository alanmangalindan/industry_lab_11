package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton, calculateHealthyWeightbutton;
    private JTextField heightInMetres, weightInKgs, bmiResult, maxHealthyWeight;


    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        this.calculateBMIButton = new JButton("Calculate BMI");
        this.calculateHealthyWeightbutton = new JButton("Calculate Healthy Weight");
        this.heightInMetres = new JTextField(15);
        this.weightInKgs = new JTextField(15);
        this.bmiResult = new JTextField(15);
        this.maxHealthyWeight = new JTextField(15);

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        JLabel heightLabel = new JLabel("Height in metres: ");
        JLabel weightLabel = new JLabel("Weight in Kilograms: ");
        JLabel bmiResultLabel = new JLabel("Your Body Mass Index (BMI) is: ");
        JLabel maxWeightLabel = new JLabel("Maximum Healthy Weight for your Height: ");

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
//        setLayout(new BorderLayout());
        this.add(heightLabel);
        this.add(heightInMetres);
        this.add(weightLabel);
        this.add(weightInKgs);
        this.add(calculateBMIButton);
        this.add(bmiResultLabel);
        this.add(bmiResult);
        this.add(calculateHealthyWeightbutton);
        this.add(maxWeightLabel);
        this.add(maxHealthyWeight);

        // TODO Add Action Listeners for the JButtons
        calculateBMIButton.addActionListener(this);
        calculateHealthyWeightbutton.addActionListener(this);

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be displayed in the text box.
        if (event.getSource() == calculateBMIButton) {
            try {
                double height = Double.parseDouble(heightInMetres.getText());
                double weight = Double.parseDouble(weightInKgs.getText());
                double bmi = roundTo2DecimalPlaces((weight/(height * height)));
                bmiResult.setText(bmi + "");
            } catch (NumberFormatException|NullPointerException e) {
                System.out.println("Error: " + e.getMessage());
            }

        }
        else if (event.getSource() == calculateHealthyWeightbutton) {
            try {
                double height = Double.parseDouble(heightInMetres.getText());
                double maxHealthyBMI = 24.9;
                double maxWeightResult = roundTo2DecimalPlaces(maxHealthyBMI * height * height);
                maxHealthyWeight.setText(maxWeightResult + "");
            } catch (NumberFormatException|NullPointerException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }

    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {

        double d = Math.round(amount * 100);
        d = d/100;
        System.out.println(d);

        return ((double) Math.round(amount * 100)) / 100;
    }

}